/**
 * Created by yi.weng on 2018-10-8.
 */
let path = require('path'),
    webpack = require('webpack'),
    merge = require('webpack-merge'),
    baseConfig = require('./webpack.config.base.js')
//module.exports.serve = {
//    port: 8080,
//    add: app => {
//        app.use(convert(history()))
//    }
//};
module.exports = merge(baseConfig, {
    devServer: {
        // inline: true,
        port: 8080
    },
    // 插件配置
    plugins: []

})