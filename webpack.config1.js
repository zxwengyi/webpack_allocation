/**
 * Created by yi.weng on 2018-9-28.
 */
// 基于node的 遵循commonjs规范的
let path = require('path');//node的模块
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const CleanWebpackPlugin = require('clean-webpack-plugin'); //引入清除文件插件
//const UglifyJsPlugin  =  require('uglifyjs-webpack-plugin');
const ParallelUglifyPlugin = require('webpack-parallel-uglify-plugin');
const optimizeCss = require('optimize-css-assets-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin') //拷贝文件和文件夹
//let production = process.env.NODE_ENV === 'production';
let dev = process.env.NODE_ENV === 'development';
console.log('当前打包环境：\n *************author: reamd**************** \n dev:'+dev);

if (dev) {
    module.exports.serve = {
        port: 8080,
        add: app => {
            app.use(convert(history()))
        }
    }
}
module.exports = {
    mode: dev ? 'development' : 'production',
    devtool: dev ? 'cheap-module-eval-source-map' : 'hidden-source-map',
    entry: './src/index.js',
    //entry: ['babel-polyfill','./src/index.js'],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].js',
        publicPath: ''
    },
    module:{rules: [
        {
            test: /\.html$/,
            use: [
                {
                    loader: "html-loader",
                    options: { minimize: true }
                }
            ]
        },
        {
            test:/\.css$/,
            use: [
                {
                    loader:MiniCssExtractPlugin.loader,
                    options:{
                        publicPath: './'
                    }
                },
                "css-loader"
            ]
        },
        {
            // 图片加载器
            test:/\.(png|jpg|gif|jpeg)$/,
            loader:'url-loader?limit=10000'
        }
    ]}, // 模块配置
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html',
            chunksSortMode: 'none',
            minify: {
                removeComments: true,
                collapseWhitespace: true,
                removeAttributeQuotes: true
            }
        }),
        new MiniCssExtractPlugin({
            filename: "[name].[chunkhash:8].css",
            chunkFilename: "[id].css"
        }),
        new CleanWebpackPlugin(['dist']),//实例化，参数为目录
        //new UglifyJsPlugin(), //压缩js
        new ParallelUglifyPlugin({
            cacheDir: '.cache/',
            output: {
                /*
                 是否输出可读性较强的代码，即会保留空格和制表符，默认为输出，为了达到更好的压缩效果，
                 可以设置为false
                 */
                beautify: false,
                /*
                 是否保留代码中的注释，默认为保留，为了达到更好的压缩效果，可以设置为false
                 */
                comments: false
            },
            compress: {
                /*
                 是否在UglifyJS删除没有用到的代码时输出警告信息，默认为输出，可以设置为false关闭这些作用
                 不大的警告
                 */
                warnings: false,

                    /*
                     是否删除代码中所有的console语句，默认为不删除，开启后，会删除所有的console语句
                     */
                    drop_console: true,

                    /*
                     是否内嵌虽然已经定义了，但是只用到一次的变量，比如将 var x = 1; y = x, 转换成 y = 5, 默认为不
                     转换，为了达到更好的压缩效果，可以设置为false
                     */
                    collapse_vars: true,

                    /*
                     是否提取出现了多次但是没有定义成变量去引用的静态值，比如将 x = 'xxx'; y = 'xxx'  转换成
                     var a = 'xxxx'; x = a; y = a; 默认为不转换，为了达到更好的压缩效果，可以设置为false
                     */
                    reduce_vars: true
                }
        }),
        new optimizeCss({
            cssProcessor: require('cssnano'),
            cssProcessorOptions: {
                discardComments: { removeAll: true }
            },
            canPrint: true
        }),
        new webpack.DllReferencePlugin({
            context: __dirname,
            manifest: require('./build/manifest.json'),
        }),
        new CopyWebpackPlugin([
            {
                from: './build/vendors.js',
                to: './lib/'
            }
        ]),
        //["transform-runtime", {
        //    "helpers": true, // default
        //    "polyfill": true, // default
        //    "regenerator": true, // defalut
        //    "moduleName": "babel-runtime" // default
        //}]
    ],
    mode:'development', // 可以更改模式
    resolve:{}, // 配置解析
}
// 在webpack中如何配置开发服务器 webpack-dev-server
