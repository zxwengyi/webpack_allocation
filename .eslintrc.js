/**
 * Created by yi.weng on 2018-10-18.
 */
/**
 * 自定义的ESLint配置项
 * 规则中文 @see http://eslint.cn/docs/user-guide/configuring
 * 规则英文 @see https://eslint.org/docs/user-guide/configuring
 *
 * 使用注释自定义规则 @see https://eslint.org/docs/user-guide/configuring#disabling-rules-with-inline-comments
 */
module.exports = {
    root: true,
    parserOptions: {
        sourceType: 'module'
    },
    env: {
        browser: true,
    },
    rules: {
        // 必须使用 === 或 !==，禁止使用 == 或 !=，与 null 比较时除外
        // @warn 在异步接口返回时不确定参数是数值还是字符串，有时可利用这个类型转换
        'eqeqeq': 'warn',
        // 禁止使用没必要的 {} 作为代码块
        // @off 有时候需要用代码块做逻辑区分
        'no-lone-blocks': 'off',
        // 文件最后一行必须有一个空行
        // @error 应该在文件末尾保持一个换行
        'eol-last': 'error',
        // 代码块嵌套的深度禁止超过 10 层
        // @warn 有些特殊情况会出现  警示即可
        'max-depth': [
            'warn',
            10
        ],
        // 定义过的变量必须使用
        // @warn 多文件互相引用时 偶尔会出现无引用的情况
        'no-unused-vars': [
            'warn',
            {
                vars: 'all',
                args: 'none',
                caughtErrors: 'none',
                ignoreRestSiblings: true
            }
        ],
        // 函数的参数禁止超过10个
        // @warn 警示即可
        'max-params': ['warn', 10],
        // 回调函数嵌套禁止超过 5 层
        // @warn 警示即可
        'max-nested-callbacks': ['warn', 5],
        // 循环内的函数中不能出现循环体条件语句中定义的变量
        // @warn 警示即可
        'no-loop-func': 'warn',
        // Promise 的 reject 中必须传入 Error 对象
        // @off 不需要限制
        'prefer-promise-reject-errors': 'off',
    }
};
