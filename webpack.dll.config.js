/**
 * Created by yi.weng on 2018-9-29.
 */
const webpack = require('webpack')
const library = '[name]_lib'
const path = require('path')
const CleanWebpackPlugin = require('clean-webpack-plugin'); //引入清除文件插件
module.exports = {
    mode: 'production',
    entry: {
        vendors: ['jquery','vue']
    },

    output: {
        filename: '[name].js',
        path: path.join(__dirname, 'build'),
        library
    },

    plugins: [
        new CleanWebpackPlugin(['build']),//实例化，参数为目录
        new webpack.DllPlugin({
            path: path.join(__dirname, 'build','manifest.json'),
            // This must match the output.library option above
            name: library,
            context: __dirname
        }),
    ],
}