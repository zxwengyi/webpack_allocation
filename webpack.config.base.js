/**
 * Created by yi.weng on 2018-9-28.
 */
// 基于node的 遵循commonjs规范的
let path = require('path');//node的模块
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
//const UglifyJsPlugin  =  require('uglifyjs-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin') //拷贝文件和文件夹
const optimizeCss = require('optimize-css-assets-webpack-plugin');
//let production = process.env.NODE_ENV === 'production';
let dev = process.env.NODE_ENV === 'development';
console.log('当前打包环境：\n *************author: reamd**************** \n dev:'+dev);
module.exports = {
    mode: dev ? 'development' : 'production',
    devtool: dev ? 'cheap-module-eval-source-map' : 'hidden-source-map',
    entry: {
        index: './src/index.js'
    },
    //entry: ['babel-polyfill','./src/index.js'],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].js',
        publicPath: ''
    },
    module:{rules: [
        {
            test: /(\.js)$/,
            exclude:/node_modules/,
            use: {
                loader: "babel-loader"
            }
        },
        {
            test: /\.js$/,
            enforce: 'pre',
            include: [path.resolve(__dirname, 'src')],
            loader: 'eslint-loader',
            options: {
                formatter: require('eslint-friendly-formatter'),
            }
        },
        {
            test: /\.html$/,
            use: [
                {
                    loader: "html-loader",
                    options: { minimize: true }
                }
            ],
            exclude:'/node_modules/'
        },
        {
            test:/\.css$/,
            use: [
                {
                    loader:MiniCssExtractPlugin.loader,
                    options:{
                        publicPath: './'
                    }
                },
                "css-loader"
            ],
            exclude:'/node_modules/'
        },
        {
            // 图片加载器
            test:/\.(png|jpg|gif|jpeg)$/,
            loader:'url-loader?limit=10000',
            exclude:'/node_modules/'
        }
    ]}, // 模块配置
    plugins: [
        new CopyWebpackPlugin([
            {
                from: './build/vendors.js',
                to: './lib/'
            }
        ]),
        new HtmlWebpackPlugin({
            template: './src/index.html',
            chunksSortMode: 'none',
            minify: {
                removeComments: true,
                collapseWhitespace: true,
                removeAttributeQuotes: true
            }
        }),
        new MiniCssExtractPlugin({
            filename: "[name].[chunkhash:8].css",
            chunkFilename: "[id].css"
        }),
        new optimizeCss({
            cssProcessor: require('cssnano'),
            cssProcessorOptions: {
                discardComments: { removeAll: true }
            },
            canPrint: true
        }),
        new webpack.DllReferencePlugin({
            context: __dirname,
            manifest: require('./build/manifest.json'),
        }),
    ],
    mode:'development', // 可以更改模式
    resolve:{}, // 配置解析
}
// 在webpack中如何配置开发服务器 webpack-dev-server
