/**
 * Created by yi.weng on 2018-9-28.
 */
    /*多页面配置*/
let path = require('path');//node的模块
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const CleanWebpackPlugin = require('clean-webpack-plugin'); //引入清除文件插件
const UglifyJsPlugin  =  require('uglifyjs-webpack-plugin');
const optimizeCss = require('optimize-css-assets-webpack-plugin');
const dev = Boolean(!process.env.WEBPACK_SERVE)
console.log("==============="+dev);
if (dev) {
    module.exports.serve = {
        port: 8080,
        add: app => {
            app.use(convert(history()))
        }
    }
}
module.exports = {
    mode: dev ? 'development' : 'production',
    devtool: dev ? 'cheap-module-eval-source-map' : 'hidden-source-map',
    entry: {
        pag1: './src/pag1/index.js',
        pag2: './src/pag2/index.js'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: './[name]/[name].js'
    },
    module:{rules: [
        {
            test: /\.html$/,
            use: [
                {
                    loader: "html-loader",
                    options: { minimize: true }
                }
            ]
        },
        {
            test:/\.css$/,
            use: [
                {
                    loader:MiniCssExtractPlugin.loader,
                    options:{
                        publicPath: './'
                    }
                },
                "css-loader"
            ]
        }
    ]},
    plugins: [
        new HtmlWebpackPlugin({
            title:'测试',
            template: './src/pag1/pag1.html',
            chunksSortMode: 'none',
            filename: './pag1/pag1.html',
            inject: true,
            chunks:['pag1'],
            minify: {
                removeComments: true,
                collapseWhitespace: true
            }
        }),
        new HtmlWebpackPlugin({
            filename: './pag2/pag2.html',
            chunksSortMode: 'none',
            template: './src/pag2/pag2.html',
            inject: true,
            chunks:['pag2'],//指定需要引入的js文件
            minify: {
                removeComments: true,
                collapseWhitespace: true
            }
        }),
        new MiniCssExtractPlugin({
            filename: "./[name]/[name].[chunkhash:8].css",
            chunkFilename: "[id].css"
        }),
        new CleanWebpackPlugin(['dist']),//实例化，参数为目录
        new UglifyJsPlugin(), //压缩js
        new optimizeCss({
            cssProcessor: require('cssnano'),
            cssProcessorOptions: {
                discardComments: { removeAll: true }
            },
            canPrint: true
        })
    ],
    resolve:{}, // 配置解析
}