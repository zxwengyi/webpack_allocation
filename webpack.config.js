/**
 * Created by yi.weng on 2018-10-9.
 */
module.exports = {
    // 项目代码检查设置  一般不改动
    lint: {
        // HtmlHint
        html: {
            open: false,
            // 配置文件 ，路径相对于当前根目录 /webpack
            configFile: '/.htmlhintrc'
        }
    },
    // JS模块输出路径，提取的css，img目录将与该路径同级，相对于当前根目录 /webpack
    outputPath: './dist/js',
}