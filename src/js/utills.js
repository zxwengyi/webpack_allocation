/**
 * Created by yi.weng on 2018-10-19.
 */
import $ from 'jquery'
export default {
    getOS () {
        let ua = navigator.userAgent
        return ua.match(/Android/i)
            ? 'Android'
            : (ua.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/i)
                ? 'IOS'
                : (
                (typeof callHostFunction !== "undefined" || typeof this.getQuery('isExeUserAgent') === "true")
                    ? 'PC'
                    : 'Web'
            )
        )
    },
    isMobile () {
        let os = this.getOS()
        return (os === 'Android' || os === 'IOS')
    },
    event () {
        // 移动端采用zepto的事件名称
        let flag = this.isMobile()
        return {
            eStart: flag ? 'touchstart' : 'mousedown',
            eMove: flag ? 'touchmove' : 'mousemove',
            eEnd: flag ? 'touchend' : 'mouseup',
            eCancel: flag ? 'touchcancel' : 'mouseout'
        }
    },
    getDom (dom) {
        let pDom = typeof dom === 'string'? document.querySelector(dom) : dom
        try {
            pDom.on = function (evt, subDom, cb, flag) {
                if(typeof subDom === 'function'){
                    if(typeof cb === 'undefined') {
                        pDom.addEventListener(evt, subDom)
                    }else {
                        pDom.addEventListener(evt, subDom, cb)
                    }
                }else {
                    if(typeof flag === 'undefined') {
                        pDom.addEventListener(evt, function (e) {
                            let sDom = Array.prototype.slice.call(pDom.querySelectorAll(subDom))
                            sDom.forEach(function (item, idx) {
                                if(item.contains(e.target)) {
                                    cb.bind(item)(e)
                                }
                            })
                        })
                    }else {
                        pDom.addEventListener(evt, function (e) {
                            let sDom =  Array.prototype.slice.call(pDom.querySelectorAll(subDom))
                            sDom.forEach(function (item, idx) {
                                if(item.contains(e.target)) {
                                    cb.bind(item)(e)
                                }
                            })
                        }, flag)
                    }
                }
            }
        }catch (e) {
            console.log('bind element error:', dom)
            console.log(e)
        }
        return pDom
    },
    getSwipeArr (obj, cb) { //依赖jquery
        let c = {
            disableClass: obj.disableClass || 'disabled',
            direction: obj.direction || '',
            cIdx: typeof obj.cIdx === 'number'? obj.cIdx : parseInt(obj.cIdx),
            domLength: obj.domLength,
            $leftSwipe: obj.$leftSwipe,
            $rightSwipe: obj.$rightSwipe
        }
        c.$dom = c.direction === 'left'
            ? c.$leftSwipe
            : c.direction === 'right'
            ? c.$rightSwipe
            : $('body')
        if(!c.$dom.hasClass(c.disableClass)) {
            if(c.direction === 'left') {
                c.cIdx--
                c.$rightSwipe.removeClass(c.disableClass)
                if(c.cIdx === 0) {
                    c.$leftSwipe.addClass(c.disableClass)
                }
            }else if(c.direction === 'right') {
                c.cIdx++
                c.$leftSwipe.removeClass(c.disableClass)
                if(c.cIdx === c.domLength) {
                    c.$rightSwipe.addClass(c.disableClass)
                }
            }else {
                c.$leftSwipe.removeClass(c.disableClass)
                c.$rightSwipe.removeClass(c.disableClass)
                if(c.cIdx === 0) {
                    c.$leftSwipe.addClass(c.disableClass)
                }
                if(c.cIdx === c.domLength){
                    c.$rightSwipe.addClass(c.disableClass)
                }
            }
            cb()
        }
    },
    getQuery (name) {
        let reg = new RegExp("(?:^|&)"+ name +"=([^&]*)(?:&|$)");
        let res = window.location.search.substr(1).match(reg);
        if(res !== null){
            return  decodeURIComponent(res[1]);
        }
        return null;
    }
}
