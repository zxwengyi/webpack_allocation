/**
 * Created by yi.weng on 2018-9-28.
 */
import $ from 'jquery';
import './style/index.css';
import first from './views/first'
$(function(){
    /*设置rem的基础值1280宽的屏幕下，100px=1rem*/
    setTimeout(function () {
        let deviceWidth = document.documentElement.clientWidth
        console.log(deviceWidth);
         if(deviceWidth > 1280) deviceWidth = 1280
        document.documentElement.style.fontSize = deviceWidth / 12.8 + 'px'
    },0)
    window.addEventListener('resize', function () {
        setTimeout(function () {
            let deviceWidth = document.documentElement.clientWidth
             if(deviceWidth > 1280) deviceWidth = 1280
            document.documentElement.style.fontSize = deviceWidth / 12.8 + 'px'
        },0)
    })
    let e = new first();
    e.addMethdes()
})
